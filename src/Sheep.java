public class Sheep {

   enum Animal {sheep, goat}
   
   public static void main (String[] param) {
      // for debugging
   }


   
   public static void reorder (Animal[] animals) {

      Animal[] reordered = new Animal[animals.length];
      int startPointer = 0;
      int endPointer = animals.length - 1;

      for (Animal animal : animals) {
         if (animal == Animal.goat) {
            reordered[startPointer] = animal;
            startPointer ++;
         }
         else {
            reordered[endPointer] = animal;
            endPointer --;
         }
      }
      System.arraycopy(reordered, 0, animals, 0, reordered.length);
   }
}

